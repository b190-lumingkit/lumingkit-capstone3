import './App.css';

import { UserProvider } from './UserContext';
import { useEffect, useState } from 'react';
import { Navigate, BrowserRouter as Router, Route, Routes} from 'react-router-dom';


// Pages
import Home from './pages/Home';
import Error401 from './pages/Error401';
//user
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Address from './pages/user/Address';
import Profile from './pages/user/Profile';
//product
import Products from './pages/product/Products';
import Popular from './pages/product/Popular';
import Featured from './pages/product/Featured';
import Cart from './pages/product/Cart'
import UserBrew from './components/product/UserBrew';
//orders
import OrderDetails from './pages/product/OrderDetails';
//admin
import AdminDashboard from './pages/AdminDashboard';

// Components
import UserNavbar from './components/UserNavbar';
import NonUserNavbar from './components/NonUserNavbar';
import ProductView from './components/product/ProductView';
import { Container } from 'react-bootstrap';
import Error404 from './components/errors/Error404'

function App() {
  const [user, setUser] = useState('');
  const [cart, setCart] = useState([]);
  const [tempCart, setTempCart] = useState([]);
  const unsetUser = () => { 
    setCart([])
    localStorage.clear()
  }

  useEffect(() => {

    fetch(`${ process.env.REACT_APP_API_URL }/users/profile`, {
        method: "GET",
        headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
    })
    .then(res => res.json())
    .then(data => {
      (typeof data._id !== "undefined") ?
      setUser({
        id: data._id,
        isAdmin: data.isAdmin,
        email: data.email,
        firstName: data.firstName,
        lastName: data.lastName
      })
      :
      setUser({
        id: null,
        isAdmin: null,
        email: null
      });
    })

    if (localStorage.getItem('token') !== null) {
      fetch(`${process.env.REACT_APP_API_URL}/cart`, {
          method: "GET",
          headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
      })
      .then(res => res.json())
      .then(data => (data) ? setCart(data.products) : setCart([]))
    }

}, [user])

  return(
    (user !== '') ?
      <UserProvider value={{user, setUser, unsetUser, cart, setCart, tempCart, setTempCart}}>
        <Router>
          {(user.id !== null) ? <UserNavbar /> : <NonUserNavbar />}
          <Container className="main-container p-3 bg-main" fluid>
            <Routes>
              <Route path="*" element={<Error404 />} />
              <Route path='/' element={<Home />} />
              <Route path='/login' 
                element={user.id !== null ? <Navigate to='/' /> : <Login />} 
              />
              <Route path='/logout' element={<Logout />} />
              <Route path='/register'
                element={user.id !== null ? <Navigate to='/' /> : <Register />} 
              />
              {/* // user routes */}
              <Route path='/u/address/add'
                  element={user.id !== null ? <Address /> : <Login />} 
              />
              <Route path='/u/profile'
                element={user.id !== null ? <Profile /> : <Login />} 
              />
              {/* // product routes */}
              <Route path='/products' element={<Products />} />
              <Route path='/products/popular' element={<Popular />} />
              <Route path='/products/featured' element={<Featured />} />
              <Route path='/products/:productId' element={<ProductView />} />
              <Route path='/cart' element=<Cart /> />
              <Route path='/brew-your-coffee'
                  element={user.id !== null ? <UserBrew /> : <Login />} 
              />
              {/* order routes */}
              <Route path='/orders/:orderId' element=<OrderDetails /> />
              {/* isAdmin */}
              <Route path='/admin-dashboard' 
                element={user.isAdmin ? <AdminDashboard /> : <Error401 />} 
              />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
      :
      <Container></Container>
  )
}

export default App;
