import { useState, useEffect, useContext, Fragment } from "react";
import { NavLink } from 'react-router-dom';
import { Row, Col, Form, Button, Nav} from 'react-bootstrap';

import jwt_decode from "jwt-decode";

import { UserContext } from '../../UserContext';
import Swal from 'sweetalert2';

export default function LoginForm()
{
    const {setUser} = useContext(UserContext);
    const [email, setEmail] = useState(""),
        [password, setPassword] = useState(""),
        [isInputValid, setIsInputValid] = useState(false);
    
    useEffect(() => { 
        (email !== "" && password !== "") ? setIsInputValid(true) : setIsInputValid(false) 
    }, [email, password]);

    function loginUser(e)
    {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/users/login`,
        {
            method: 'POST',
            headers: {
                'Content-type':'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if (typeof data.access !== 'undefined')
            {
                localStorage.setItem('token', data.access)
                const userData = jwt_decode(data.access);
                setUser(userData);
            }
            else
            {
                Swal.fire({
                    title: "Auth failed",
                    icon: "error",
                    text: "Check your login credentials and try again."
                })
            }
        })

        setEmail("");
        setPassword("");
    }

    return(
        <Fragment>
            <Form id="loginForm" className="bg-second p-4"  onSubmit={(e => loginUser(e))}>
                <Row className='mb-2'>
                    <Col>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email"
                            value={email}
                            onChange={e => setEmail(e.target.value)} 
                            required 
                        />
                    </Col>
                </Row>
                <Row className='mb-2'>
                    <Col>
                        <Form.Control 
                            type="password" 
                            placeholder="Password"
                            value={password}
                            onChange={e => setPassword(e.target.value)} 
                            required 
                        />
                    </Col>
                </Row>
                <Row>
                    <Col className="d-grid">
                        <Button className="btn-accent" type="submit">Login</Button>
                    </Col>
                </Row>    
            </Form>
            <Row>
                <Col className="text-center p-2">
                    <Nav.Link as={ NavLink } to='/register' exact='true'>
                        Need an account? Sign up <u className="text-primary">here</u>
                    </Nav.Link>
                </Col>
            </Row>
        </Fragment>
    )
}