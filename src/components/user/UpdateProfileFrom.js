// Dependencies
import { useContext, useState } from 'react';
import { UserContext } from '../../UserContext';
import Swal from 'sweetalert2';
import {  useNavigate } from 'react-router-dom';

// Components
import { Row, Col, Form, Button } from 'react-bootstrap';
import ChangePassword from './ChangePassword';

export default function UpdateProfileForm()
{
    const { user } = useContext(UserContext)
    const [email, setEmail] = useState(user.email),
          [firstName, setFirstName] = useState(user.firstName),
          [lastName, setLastName] = useState(user.lastName),
          [password, setPassword] = useState('');
    const navigate = useNavigate();

    function handleLogout() {
        Swal.fire({
            title: "You have been logged out.",
            icon: "warning"
        })

        navigate('/logout');
    }
    function updateUser() {

        fetch(`${process.env.REACT_APP_API_URL}/users/login`,
        {
            method: 'POST',
            headers: {
                'Content-type':'application/json'
            },
            body: JSON.stringify({
                email: user.email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if (typeof data.access !== 'undefined')
            {
                fetch(`${process.env.REACT_APP_API_URL}/users/profile`,
                {
                    method: 'PUT',
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem('token')}`,
                        'Content-type':'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if (data) {
                        setPassword('');
                        Swal.fire({
                            title: "Profile updated.",
                            icon: "success"
                        })
                    }
                    else {
                        Swal.fire({
                            title: "Something went wrong.",
                            icon: "error",
                            text: "Please try again later."
                        })
                    }
                })
            }
            else
            {
                Swal.fire({
                    title: "Auth failed",
                    icon: "error",
                    text: "Check your login credentials and try again."
                })
            }
        })
    }


    return(
        <Form id="updateProfileForm" className="p-4 bg-third">
            <h5>Profile Details</h5>
            <Row className='mb-2'>
                <Col>
                    <Form.Label>First Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        value={firstName}
                        onChange={e => setFirstName(e.target.value)}
                        required
                    />
                </Col>
                <Col>
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        value={lastName}
                        onChange={e => setLastName(e.target.value)}
                        required
                    />
                </Col>
            </Row>
            <Row className='mb-2'>
                <Col>
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Email Address"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required
                    />
                </Col>
            </Row>
            <Row className='mb-2'>
                <Col>
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Your Current Password"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        required
                    />
                </Col>
            </Row>
            <Row className='mb-2'>
                <Col className="d-grid">
                    <Button className="mb-2 rounded-0" variant="primary" onClick={() => updateUser()}>
                        Update Profile
                    </Button>
                </Col>
            </Row>  
            <h5 className='mt-3'>Security</h5>  
            <Row className='mb-2'>
                <Col className="d-grid">
                    <ChangePassword />
                </Col>
            </Row> 
            <h5 className='mt-3'>Are you leaving? :(</h5>  
            <Row className='mb-2'>
                <Col className="d-grid">
                    <Button className="btn-secondary border-0 rounded-0" onClick={() => handleLogout()}>
                        Logout
                    </Button>
                </Col>
            </Row>  
        </Form>
    )
}