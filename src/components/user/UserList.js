import { useEffect, useState } from 'react';
import { Button, Row, Table } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function UserList(props)
{
    const {users, fetchUsers} = props;
    const [table, setTable] = useState('');
    
    const handleIsAdmin = (userId, status) =>
    {
        fetch(`${process.env.REACT_APP_API_URL}/admin-panel/promote-user/${userId}/${status}`,
        {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data) {
                fetchUsers();
            }
            else {
                fetchUsers();
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again later."
                });

            }
        })
    }

    useEffect(() =>
    {
        const usersTable = users.map(user =>
        {
            return(
                <tr key={user._id}>
                    <td>{user.firstName} {user.lastName}</td>
                    <td className="text-center">{user.email}</td>
                    <td className="d-flex justify-content-evenly">
                        { user.isAdmin === true ?
                            <Button
                            variant="success"
                            size="sm"
                            onClick={() => handleIsAdmin(user._id, false)}
                            >Yes</Button>
                        :
                            <Button
                            variant="secondary"
                            size="sm"
                            onClick={() => handleIsAdmin(user._id, true)}
                            >No</Button>
                        }
                    </td>
                </tr>
            )
        })

        setTable(usersTable);

    }, [users, fetchUsers])

    return(
        <Row className="mt-2">
            <Table striped bordered hover className="p-2">
                <thead>
                    <tr className='text-center'>
                        <th>Full Name</th>
                        <th>Email</th>
                        <th>Admin</th>
                    </tr>
                </thead>
                <tbody><>{table}</></tbody>
            </Table>
        </Row>
    )
}