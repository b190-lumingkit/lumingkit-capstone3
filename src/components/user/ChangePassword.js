import { useEffect, useState, useContext } from 'react';
import { Modal, Button, Form, Row, Col } from 'react-bootstrap';

import { UserContext } from '../../UserContext';
import Swal from 'sweetalert2';


export default function ChangePassword()
{
    const {user} = useContext(UserContext);
    const [showAddressModal, setShowAddressModal] = useState(false),
          [oldPassword, setOldPassword] = useState(''),
          [password1, setPassword1] = useState(''),
          [password2, setPassword2] = useState(''),
          [isInputValid, setIsInputValid] = useState(false);

    useEffect(() => {

        (password1 === password2 && oldPassword !== '' && password1 !== '') ?
        setIsInputValid(true)
        :
        setIsInputValid(false)

    }, [oldPassword, password1, password2])

    function changePassword() 
    {
        fetch(`${process.env.REACT_APP_API_URL}/users/login`,
        {
            method: 'POST',
            headers: {
                'Content-type':'application/json'
            },
            body: JSON.stringify({
                email: user.email,
                password: oldPassword
            })
        })
        .then(res => res.json())
        .then(data => {
            if (typeof data.access !== 'undefined')
            {
                fetch(`${process.env.REACT_APP_API_URL}/users/change-password`,
                {
                    method: 'PUT',
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem('token')}`,
                        'Content-type':'application/json'
                    },
                    body: JSON.stringify({
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if (data) {
                        setShowAddressModal(false);
                        setPassword1('');
                        setPassword2('');
                        setOldPassword('');  
                        Swal.fire({
                            title: "Password updated.",
                            icon: "success"
                        })
                    }
                    else {
                        Swal.fire({
                            title: "Something went wrong.",
                            icon: "error",
                            text: "Please try again later."
                        })
                    }
                })
            }
            else
            {
                Swal.fire({
                    title: "Auth failed",
                    icon: "error",
                    text: "Check your login credentials and try again."
                })
            }
        })
    }

    return(
        <>
            <div className="d-grid">
                <Button variant="danger" className="rounded-0" onClick={() => setShowAddressModal(true)}>Change Password</Button>
            </div>
            <Modal
                show={showAddressModal}
                onHide={() => setShowAddressModal(false)}
                size="lg"
                aria-labelledby="address-form-modal"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="address-form-modal">
                        Change Password
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form className="bg-light p-4">
                        <Row className='mb-2'>
                            <Col>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Current Password"
                                    value={oldPassword}
                                    onChange={e => setOldPassword(e.target.value)}
                                    required
                                />
                            </Col>
                        </Row>
                        <Row className='mb-2'>
                            <Col>
                                <Form.Control 
                                    type="password" 
                                    placeholder="New Password"
                                    value={password1}
                                    onChange={e => setPassword1(e.target.value)}
                                    required
                                />
                            </Col>
                        </Row>
                        <Row className='mb-2'>
                            <Col>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Confirm New Password"
                                    value={password2}
                                    onChange={e => setPassword2(e.target.value)}
                                    required
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col className="d-grid">
                            {   isInputValid ?
                                <Button 
                                    className="mr-auto" 
                                    variant="primary" 
                                    type="button"
                                    onClick={() => changePassword()}
                                >
                                    Change Password
                                </Button>
                                :
                                <Button className="mr-auto" variant="primary" type="button" disabled>Change Password</Button>
                            }
                            </Col>
                        </Row>    
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="light" onClick={() => setShowAddressModal(false)}>Cancel</Button>
                </Modal.Footer>
            </Modal>
        </>
    )

}