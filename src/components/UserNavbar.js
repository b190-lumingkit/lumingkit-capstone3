import { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { UserContext } from '../UserContext';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCartShopping, faUser } from '@fortawesome/free-solid-svg-icons';

export default function AppNavbar()
{
  
    const { user } = useContext(UserContext); 

    return(
        <Navbar className="position-sticky top-0 p-3 bg-accent" expand="md">
            <Navbar.Brand>
                <Nav.Link as={ NavLink } to ='/' exact='true' className="text-secondary">Kape ni Angkol</Nav.Link>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="#app-navbar"></Navbar.Toggle>
            <Navbar.Collapse id="#app-navbar" className="justify-content-end">
                <Nav>
                    <Nav.Link as={ NavLink } to ='/products' exact='true' className="text-secondary">Products</Nav.Link>
                    { (user.isAdmin) ?
                      <>
                        <Nav.Link as={ NavLink } to ='/admin-dashboard' exact='true' className="text-secondary">Dashboard</Nav.Link>
                        <Nav.Link as={ NavLink } to ='/u/profile' exact='true' className="text-secondary">
                          <FontAwesomeIcon icon={faUser} />
                        </Nav.Link>
                      </>
                      :
                      <>
                      <Nav.Link as={ NavLink } to ='/brew-your-coffee' exact='true' className="text-secondary">Brew</Nav.Link>
                      <Nav.Link as={ NavLink } to ='/cart' exact='true' className="text-secondary">
                        <FontAwesomeIcon icon={faCartShopping} />
                      </Nav.Link>
                      <Nav.Link as={ NavLink } to ='/u/profile' exact='true' className="text-secondary">
                        <FontAwesomeIcon icon={faUser} />
                      </Nav.Link>
                      </>
                    }
                    
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}