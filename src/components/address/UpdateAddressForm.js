import { useState, useEffect } from "react";
import { Row, Col, Form, Button, Accordion } from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function UpdateAddressForm({address})
{
    const [addressData, setAddressData] = useState({
        name: address.name,
        line_1: address.line_1,
        line_2: address.line_2,
        line_3: address.line_3,
        city: address.city,
        province: address.province,
    });
    const [isInputValid, setIsInputValid] = useState(false);

    useEffect(() =>
    {
        (Object.values(addressData).every(input => input !== "")) ?
        setIsInputValid(true)
        :
        setIsInputValid(false)
    }, [addressData])

    function updateAddress(addressId) {
        fetch(`${process.env.REACT_APP_API_URL}/users/addresses/${addressId}`,
        {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-type':'application/json'
            },
            body: JSON.stringify(addressData)
        })
        .then(res => res.json())
        .then(data => {
            if (data) {
                Swal.fire({
                    title: "Address updated.",
                    icon: "success"
                })
            }
            else {
                Swal.fire({
                    title: "Something went wrong.",
                    icon: "error",
                    text: "Please try again later."
                })
            }
        })
    }

    function removeAddress(addressId) {
        fetch(`${process.env.REACT_APP_API_URL}/users/addresses/${addressId}/remove`,
        {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-type':'application/json'
            },
        })
        .then(res => res.json())
        .then(data => {
            if (data) {
                Swal.fire({
                    title: "Address removed.",
                    icon: "success"
                })
            }
            else {
                Swal.fire({
                    title: "Something went wrong.",
                    icon: "error",
                    text: "Please try again later."
                })
            }
        })
    }

    return(
        <Accordion.Item eventKey={address._id}>
            <Accordion.Header>{addressData.name}</Accordion.Header>
            <Accordion.Body>
                <Form className="bg-light p-4">
                    <Row className='mb-2'>
                        <Col>
                            <Form.Label>Name</Form.Label>
                            <Form.Control 
                                type="text" 
                                value={addressData.name}
                                onChange={e => setAddressData({...addressData, name: e.target.value})}
                                required
                            />
                        </Col>
                    </Row>
                    <Row className='mb-2'>
                        <Col>
                            <Form.Label>Contact No.</Form.Label>
                            <Form.Control 
                                type="number" 
                                value={addressData.line_1}
                                onChange={e => setAddressData({...addressData, line1: e.target.value})}
                                required
                            />
                        </Col>
                    </Row>
                    <Row className="mb-2">
                        <Col>
                            <Form.Label>BLDG Name / House No.</Form.Label>
                            <Form.Control 
                                type="text" 
                                value={addressData.line_2}
                                onChange={e => setAddressData({...addressData, line2: e.target.value})}
                                required
                            />
                        </Col>
                    </Row>
                    <Row className='mb-2'>
                        <Col>
                            <Form.Label>Street</Form.Label>
                            <Form.Control 
                                type="text" 
                                placeholder="Street"
                                value={addressData.line_3}
                                onChange={e => setAddressData({...addressData, line3: e.target.value})}
                                required
                            />
                        </Col>
                    </Row>
                    <Row className='mb-2'>
                        <Col sm className="mb-2">
                            <Form.Label>City</Form.Label>
                            <Form.Control 
                                type="text" 
                                placeholder="City"
                                value={addressData.city}
                                onChange={e => setAddressData({...addressData, city: e.target.value})}
                                required
                            />
                        </Col>
                        <Col sm>
                            <Form.Label>Province</Form.Label>
                            <Form.Control 
                                type="text" 
                                placeholder="Province"
                                value={addressData.province}
                                onChange={e => setAddressData({...addressData, province: e.target.value})}
                                required
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col className="d-grid">
                        { (isInputValid) ?
                            <Button 
                                variant="primary" 
                                type="button"
                                onClick={() => updateAddress(address._id)}
                            >
                                Update Address
                            </Button>
                            :
                            <>
                            <Button 
                                variant="primary" 
                                type="button"
                                disabled
                            >
                                Update Address
                            </Button>
                            </>
                        }
                            <Button 
                                className="mt-2" 
                                variant="warning" 
                                type="button"
                                onClick={() => removeAddress(address._id)}
                            >
                                Remove Address
                            </Button>   
                        </Col>
                    </Row>    
                </Form>
            </Accordion.Body>
        </Accordion.Item>
    )
}