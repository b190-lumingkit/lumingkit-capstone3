import { useState } from 'react';
import { Modal, Button} from 'react-bootstrap';

import AddressForm from '../../components/address/AddressForm';

export default function AddressModal()
{
    const [showAddressModal, setShowAddressModal] = useState(false);

    return(
        <>
            <div className="d-grid">
                <Button className="bg-accent mt-2 border-0 rounded-0" onClick={() => setShowAddressModal(true)}>Add Address</Button>
            </div>
            <Modal
                show={showAddressModal}
                onHide={() => setShowAddressModal(false)}
                size="lg"
                aria-labelledby="address-form-modal"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="address-form-modal">
                        Add Address
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <AddressForm />
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="light" onClick={() => setShowAddressModal(false)}>Cancel</Button>
                </Modal.Footer>
            </Modal>
        </>
    )

}