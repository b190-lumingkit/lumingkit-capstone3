import { useState, useEffect, useContext, Fragment } from "react";
import { Navigate, useNavigate, Link, NavLink } from 'react-router-dom';
import { Row, Col, Form, Button, Nav} from 'react-bootstrap';

import { UserContext } from '../../UserContext';
import Swal from 'sweetalert2';

export default function AddressForm()
{
    const {user} = useContext(UserContext);
    const [line1, setLine1] = useState(""),
          [line2, setLine2] = useState(""),
          [line3, setLine3] = useState(""),
          [city, setCity] = useState(""),
          [province, setProvince] = useState(""),
          [isInputValid, setIsInputValid] = useState(false);
    const inputs = {
        line1: line1,
        line2: line2,
        line3: line3,
        city: city,
        province: province
    }
 

    useEffect(() =>
    {
        (Object.values(inputs).every(input => input !== "")) ?
        setIsInputValid(true)
        :
        setIsInputValid(false)
    }, [inputs])

    function newAddress(e)
    {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/users/addresses`,
        {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-type':'application/json'
            },
            body: JSON.stringify({
                line_1: line1,
                line_2: line2,
                line_3: line3,
                city: city,
                province: province,
            })
        })
        .then(res => res.json())
        .then(data =>
        {
            console.log()
            if (data.hasOwnProperty('auth'))
            {
                Swal.fire({
                    title: "Something went wrong.",
                    icon: "error",
                    text: "You need to login to perform this action."
                })
            }
            else if (data)
            {
                Swal.fire({
                    title: "Address added.",
                    icon: "success"
                })

                document.querySelector('#addressForm').reset();
            }
            else
            {
                Swal.fire({
                    title: "Something went wrong.",
                    icon: "error",
                    text: "Please try again later."
                })
            }
        })
    }

    return(
        <Fragment>
            <Form id="addressForm" onSubmit={(e => newAddress(e))} className="bg-light p-4">
                <Row className='mb-2'>
                    <Col>
                        <Form.Control 
                            type="number" 
                            placeholder="Contact No."
                            value={line1}
                            onChange={e => setLine1(e.target.value)}
                            required
                        />
                    </Col>
                    <Col>
                        <Form.Control 
                            type="text" 
                            placeholder="BLDG Name / House No."
                            value={line2}
                            onChange={e => setLine2(e.target.value)}
                            required
                        />
                    </Col>
                </Row>
                <Row className='mb-2'>
                    <Col>
                        <Form.Control 
                            type="text" 
                            placeholder="Street"
                            value={line3}
                            onChange={e => setLine3(e.target.value)}
                            required
                        />
                    </Col>
                </Row>
                <Row className='mb-2'>
                    <Col sm className="mb-2">
                        <Form.Control 
                            type="text" 
                            placeholder="City"
                            value={city}
                            onChange={e => setCity(e.target.value)}
                            required
                        />
                    </Col>
                    <Col sm>
                        <Form.Control 
                            type="text" 
                            placeholder="Province"
                            value={province}
                            onChange={e => setProvince(e.target.value)}
                            required
                        />
                    </Col>
                </Row>
                <Row>
                    <Col className="d-grid">
                    {   isInputValid ?
                        <Button className="mr-auto" variant="primary" type="submit">Submit</Button>
                        :
                        <Button className="mr-auto" variant="primary" type="submit" disabled>Submit</Button>
                    }
                    </Col>
                </Row>    
            </Form>
        </Fragment>        
    )
}