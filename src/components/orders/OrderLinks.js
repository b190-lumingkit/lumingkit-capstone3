import { useContext } from 'react';
import { ListGroup, Badge } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

import { UserContext } from '../../UserContext';

export default function OrderLinks({order})
{
    const {_id, status, purchasedOn} = order;
    const navigate = useNavigate();
    const date = new Date(purchasedOn);

    const handleViewOrder = () =>
    {
        navigate(`/orders/${_id}`)
    }
    
    return(
        <ListGroup.Item 
            onClick={() => handleViewOrder(_id)}
            className="d-flex justify-content-between"
        >   
            <div>
                <p>
                    <strong>Order ID:</strong> 
                    {_id}
                </p>
                <small className="text-left">
                    <strong>Date Ordered: </strong>
                    {date.getMonth()+1},  
                    {date.getDate()},
                    {date.getFullYear()}
                </small>
            </div>
            <h5>
                <Badge
                    className={`border rounded ${(status !== 'complete') ? 'bg-warning' : 'bg-success'}`}
                >
                    {status[0].toUpperCase() + status.slice(1)}
                </Badge>
            </h5>
        </ListGroup.Item>
    )
}