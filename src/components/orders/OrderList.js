import { useEffect, useState } from 'react';
import { Col, Modal, Button, Row, Table, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function OrderList(props)
{
    const {orders, fetchOrders} = props;
    const [table, setTable] = useState(''),
          [show, setShow] = useState(false),
          [orderId, setOrderId] = useState(''),
          [totalPrice, setTotalPrice] = useState(''),
          [products, setProducts] = useState(''),
          [address, setAddress] = useState(''),
          [data, setData] = useState([]),
          [sortType, setSortType] = useState('oldest'),
          [status, setStatus] = useState('pending');

    function handleShowModal(orderId)
    {
        fetch(`${process.env.REACT_APP_API_URL}/users/orders/${orderId}`)
        .then(res => res.json())
        .then(data => {
            setStatus(data.status)
            setOrderId(data._id);
            setTotalPrice(data.totalPrice.toFixed(2));
            if (data.products !== undefined) {
                setProducts(data.products.map((product, index) => 
                {
                    return(
                        <li key={index} className="d-flex justify-content-between">
                            <span>{product.name}</span>
                            <span>{product.price.toFixed(2)}</span>
                        </li>
                    )
                }));
            }
            setAddress(data.address);
        })

        setShow(true);
    }

    function isLineEmpty(line)
    {
        return line !== undefined ? `${line},` : ''; 
    }
    
    function handleCloseModal() 
    {
        setShow(false);
		setOrderId('');
        setTotalPrice('');
        setProducts('');
        setAddress('');
    }
    
    const handleIsComplete = (userId, status) =>
    {
        fetch(`${process.env.REACT_APP_API_URL}/users/orders/${userId}/${status}`,
        {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data) {
                fetchOrders();
            }
            else {
                fetchOrders();
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again later."
                });

            }
        })
    }

    

    useEffect(() =>
    {
        let sorted = [...orders];
        if (sortType === 'latest') {
                sorted = [...orders].sort((a, b) => new Date(b.purchasedOn) - new Date(a.purchasedOn))
                setData(sorted)
            }
            else {
                setData(sorted)
        }

    }, [orders, fetchOrders, sortType])

    return(
        <Row className="mt-2">
            <Col xs lg="12">
                <Row>
                    <Col>
                        <Form.Select size="sm" 
                            onChange={(e) => setSortType(e.target.value)}
                        >
                            <option value="oldest">Oldest</option>
                            <option value="latest">Latest</option>
                        </Form.Select>
                    </Col>
                </Row>
                <Table striped bordered hover className="p-2 mt-2">
                    <thead>
                        <tr className='text-center'>
                            <th>Order ID</th>
                            <th>Total</th>
                            <th>Date Purchased</th>
                            <th>Completed</th>
                        </tr>
                    </thead>
                    <tbody>
                        { data.map(order => {
                                return(
                                    <tr key={order._id}>
                                        <td onClick={() => handleShowModal(order._id)}>{order._id}</td>
                                        <td className="text-center">{order.totalPrice.toFixed(2)}</td>
                                        <td className="text-center">{order.purchasedOn}</td>
                                        <td className="d-flex justify-content-evenly">
                                            { order.status === 'complete' ?
                                                <Button
                                                variant="success"
                                                size="sm"
                                                onClick={() => handleIsComplete(order._id, 'cancelled')}
                                                >Yes</Button>
                                            :
                                                <Button
                                                variant="secondary"
                                                size="sm"
                                                onClick={() => handleIsComplete(order._id, 'complete')}
                                                >No</Button>
                                            }
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>
            </Col>

            <Modal show={show} 
                   onHide={() => setShow(false)} 
                   centered
            >
                    <Modal.Header closeButton>
                        <Modal.Title>Order Details</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Col className="mb-2">
                            <h3 className='text-center'>Order Details</h3>
                            <p>Order ID: {orderId}</p>
                            <ul>
                                {products}
                            </ul>
                            <p>Total: {totalPrice}</p>
                            <p>Address:
                                {` ${isLineEmpty(address.name)}
                                ${isLineEmpty(address.line_1)}
                                ${isLineEmpty(address.line_2)}
                                ${isLineEmpty(address.line_3)}
                                ${isLineEmpty(address.city)}
                                ${isLineEmpty(address.province)}
                                `}
                            </p>
                            <p>Status: {status[0].toUpperCase() + status.slice(1)}</p>
                        </Col> 
                    </Modal.Body>
            </Modal>
        </Row>
    )
}