import { useEffect, useState, useContext } from 'react';
import { Modal, Button, Form, Row, Col } from 'react-bootstrap';

import { UserContext } from '../../UserContext';
import Swal from 'sweetalert2';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons';

export default function AddProductReview({orderId, productNames})
{

    const [show, setShow] = useState(false),
          [description, setDescription] = useState(''),
          [isInputValid, setIsInputValid] = useState(false);


    const [rating, setRating] = useState(4);
    const [hover, setHover] = useState(0);

    useEffect(() => {

        (rating !== '') ? setIsInputValid(true) : setIsInputValid(false)

    }, [rating, description])

    function submitReview() 
    {
        fetch(`${process.env.REACT_APP_API_URL}/products/reviews/${orderId}`,
        {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-type':'application/json'
            },
            body: JSON.stringify({
                rating: (rating+1),
                products: productNames,
                description: description
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data.every(el => el === true)) {
                Swal.fire({
                    title: "Review submitted.",
                    icon: "success"
                })
                setDescription('')
                setShow(false);
            }
            else {
                Swal.fire({
                    title: "Something went wrong.",
                    icon: "error",
                    text: "Please try again later."
                })
            }
        })
    }

    return(
        <>
            <div className="d-grid">
                <Button className="border-0 rounded-0 btn-accent" onClick={() => setShow(true)}>Add Review</Button>
            </div>
            <Modal
                show={show}
                onHide={() => setShow(false)}
                size="lg"
                aria-labelledby="address-form-modal"
                centered
                className="border-0 rounded-0"
            >
                <Modal.Header closeButton className="border-0 rounded-0">
                    <Modal.Title id="address-form-modal">
                        Add Review
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body  className="border-0 rounded-0 p-0">
                    <Form className="bg-main p-4">
                        <Row className='mb-2'>
                            <Col>
                                <p>How would you rate your order?</p>
                                <div className="star-rating">
                                    {[...Array(5)].map((star, index) => {        
                                        return (         
                                            <span
                                                key={index}
                                                className={index <= (hover || rating) ? "on star" : "off star"}
                                                onClick={() => setRating(index)}
                                                onMouseEnter={() => setHover(index)}
                                                onMouseLeave={() => setHover(index)}
                                            >
                                                <FontAwesomeIcon icon={faStar} />
                                            </span>
                                        );
                                    })}
                                </div>
                            </Col>
                        </Row>
                        <Row className='mb-2'>
                            <Col>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Were you satisfied?"
                                    value={description}
                                    onChange={e => setDescription(e.target.value)}
                                    required
                                />
                            </Col>
                        </Row>   
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button className="border-0 rounded-0 btn-secondary" onClick={() => setShow(false)}>Cancel</Button>
                    <Button className="border-0 rounded-0 btn-accent"
                    type="button"
                    onClick={() => submitReview()}
                    >
                        Submit Review
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    )

}