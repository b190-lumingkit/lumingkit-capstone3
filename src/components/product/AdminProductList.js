import { useEffect, useState } from 'react';
import {Button, Row, Modal, Table, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function ProductList(props)
{
    const {products, fetchProducts} = props;
    const [table, setTable] = useState(''),
          [productId, setProductId] = useState(''),
          [description, setDescription] = useState(''),
          [name, setName] = useState(''),
          [price, setPrice] = useState(''),
          [show, setShow] = useState(false);

    function handleShowModal(productId)
    {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then(res => res.json())
        .then(data => {
            
            setProductId(data._id);
            setName(data.name);
            setDescription(data.description)
            setPrice(data.price)
        })

        setShow(true);
    }

    const handleCloseModal = () =>
    {
        setShow(false);
		setName("");
		setDescription("");
		setPrice(0);
    }
    
    const handleUpdateProduct = (e, productId) =>
    {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`,
        {
            method: 'PUT',
            headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if (data)
            {
                fetchProducts();

                Swal.fire({
					title: "Success",
					icon: "success",
					text: "Course successfully updated."
				});

                handleCloseModal();
            }
            else
            {
                fetchProducts();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again later."
				});

            }
        })
    }

    const handleIsProductAvailable = (productId, status) =>
    {
        //console.log(status)
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/${status}`,
        {
            method: 'PUT',
            headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
        })
        .then(res => res.json())
        .then(data => fetchProducts())
    }

    useEffect(() =>
    {
        const productsTable = products.map(product =>
        {
            return(
                <tr key={product._id}>
                    <td onClick={() => handleShowModal(product._id)}>{product.name}</td>
                    <td className="text-center">{product.price.toFixed(2)}</td>
                    <td className="text-center">{Math.round(product.rating)}</td>
                    <td className="d-flex justify-content-evenly">
                        { product.isAvailable === true ?
                            <Button
                            variant="success"
                            size="sm"
                            onClick={() => handleIsProductAvailable(product._id, false)}
                            >Yes</Button>
                        :
                            <Button
                            variant="secondary"
                            size="sm"
                            onClick={() => handleIsProductAvailable(product._id, true)}
                            >No</Button>

                        }
                    </td>
                </tr>
            )
        })

        
        setTable(productsTable);

    }, [products, fetchProducts])

    return(
        <Row className="mt-2">
            <h5>Product List</h5>
            <span className="text-muted text-center">Click product title to edit.</span>
            <Table striped bordered hover className="p-2">
                <thead>
                    <tr className='text-center'>
                        <th>Product Title</th>
                        <th>Price</th>
                        <th>Rating</th>
                        <th>Is Available?</th>
                    </tr>
                </thead>
                <tbody><>{table}</></tbody>
            </Table>

            {/* Update Modal */}
            <Modal show={show} 
                   onHide={() => setShow(false)} 
                   centered
            >
                <Form onSubmit={e => handleUpdateProduct(e, productId)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Update Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        
                            <Form.Group controlId="productName">
                                <Form.Label>Product Name</Form.Label>
                                <Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
                            </Form.Group>
                            <Form.Group controlId="productDescription">
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
                            </Form.Group>
                            <Form.Group controlId="productName">
                                <Form.Label>Price</Form.Label>
                                <Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
                            </Form.Group>
                        
                    </Modal.Body>
                    <Modal.Footer>
						<Button variant="secondary" onClick={handleCloseModal}>Cancel</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
                </Form>
            </Modal>
        </Row>
    )
}