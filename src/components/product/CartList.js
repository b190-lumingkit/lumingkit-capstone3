import { useContext, useState, useEffect } from 'react';
import { Row, Col, Form, Badge } from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';

import { UserContext } from '../../UserContext';
import { cartAPI } from '../utils/FetchUtils';


export default function CartList(props)
{
    const {user, cart, setCart, tempCart, setTempCart} = useContext(UserContext);
    const length = (user.id !== null) ? cart.length : tempCart.length;

    const [total, setTotal] = useState(0),
          [checkedState, setCheckedState] = useState(
            new Array(length).fill(false)
          );
    
    const handleOnChange = (position) => {

        const updatedCheckedState = checkedState.map((item, index) => 
            index === position ? !item: item
        );
        setCheckedState(updatedCheckedState);

        const totalPrice = updatedCheckedState.reduce(
        (sum, currentState, index) => {
            if (currentState === true) {
                return sum + (props.items[index].price * props.items[index].badge);
            }
            return sum;
        }, 0);

        setTotal(totalPrice)
    }

    function handleUpdateCart(userCart) {
        fetch(`${cartAPI}/update`,
        {
            method: 'PUT',
            headers: { 
                Authorization: `Bearer ${ localStorage.getItem('token') }`,'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                products: userCart
            })
        });
    }

    function updateItem(itemId, isAdd) {
        const newCart = updateItemQuantity(itemId, isAdd);

        if (user.id !== null) {
            handleUpdateCart(newCart);
            setCart(newCart);
        }
        else {
           setTempCart(newCart);
        }
    }

    function updateItemQuantity(itemId, isAdd) 
    {
        let index = props.items.findIndex(item => item._id === itemId);
        let itemPrice = props.items[index].price; 
        let badge = props.items[index].badge
        let newCart = (user.id !== null) ? [...cart] : [...tempCart];
        let cartIndex = newCart.findIndex(item => item === itemId);

        if (isAdd) {
            newCart = [...newCart, itemId];
            (checkedState[cartIndex]) ? setTotal((total + itemPrice)) : setTotal(total);
        }
        else {
            newCart.splice(cartIndex, 1);
            (checkedState[cartIndex]) ? setTotal((total - itemPrice)) : setTotal(total);

        }
        return newCart;
    }


    return(
        <>
            { props.items.map(({_id, name, badge}, index) => {
                return(
                    <Row key={_id} className="justify-content-md-center">
                        <Col xs lg="6" className="d-flex border m-1 justify-content-between">
                            <Form.Group 
                                className="d-flex m-1 p-2">
                                <Form.Check 
                                    type="checkbox"
                                    id={`custom-checkbox-${index}`}
                                    name={name}
                                    value={name}
                                    label={name}
                                    checked={checkedState[index]}
                                    onChange={() => handleOnChange(index)}
                                />
                                <Badge className="mx-2" bg="warning" text="dark">{badge}</Badge>
                            </Form.Group>
                            <div className="d-flex justify-content-between align-items-center">
                                <FontAwesomeIcon 
                                    icon={faPlus} 
                                    onClick={() => updateItem(_id, true)}
                                    className="m-3"
                                />
                                <FontAwesomeIcon 
                                    icon={faMinus} 
                                    onClick={() => updateItem(_id, false)}
                                />
                            </div>
                        </Col>
                    </Row>
                )
            })}
            <Row className="justify-content-md-center">
                <Col xs lg="6">
                    <h5 className="text-end">Total: {total.toFixed(2)}</h5>
                </Col>
            </Row>
        </>
    )
}