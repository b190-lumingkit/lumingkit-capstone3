import { useEffect, useState } from 'react';
import { Col, Card, Row } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import AddToCartButton from '../utils/AddToCartButton';


const random = Math.floor(Math.random() * 9) + 1;
const icedRand = Math.floor(Math.random() * 4) + 1;

export default function ProductCard({productProp})
{
    const {name, description, price, _id, ingredients, rating} = productProp;
    const [rndInt, setRndInt] = useState(0);
    const [showOverlay, setShowOverlay] = useState(false);


    const navigate = useNavigate();

    const handleViewProduct = () =>
    {
        navigate(`/products/${_id}`)
    };

    useEffect(() => 
    {
        const random = Math.floor(Math.random() * 9) + 1;
        const icedRand = Math.floor(Math.random() * 4) + 1;
        (name.includes('Iced')) ? setRndInt(`iced-${icedRand}`) : setRndInt(random);
    }, [name])

    return(
        <Col sm="12" md="4" className="my-5 d-flex justify-content-center">
            <Card style={{ width: '24rem' }} className="product-card">
                <div onMouseOver={() => setShowOverlay(true)} onMouseLeave={() => setShowOverlay(false)}>
                    {/* <Card.Img variant="top" src={`/coffee/${rndInt}.png`} /> */}
                    <LazyLoadImage src={`/coffee/${rndInt}.png`} width="100%" alt="coffee-icon" />
                    <Card.ImgOverlay className="d-flex flex-column justify-content-between p-0">
                        <div className="d-flex justify-content-end p-2">
                            <div className="price-badge p-2 ">₱ {price.toFixed(2)}</div>
                        </div>
                        <div className="card-overlay p-2">
                            <Card.Title className="text-center"
                                onClick={() => handleViewProduct()} 
                            >{name}
                            </Card.Title>
                            <div className={showOverlay ? "show-card-overlay" : "hide-card-overlay"}>
                                <div className="my-2">{description}</div>
                                <div className="my-2">
                                    <span><strong>Ingredients: </strong></span>
                                    {ingredients.map((item, index) =>
                                    {
                                        return(
                                            <span key={index}>{item.name}, </span>
                                        )
                                    })}
                                </div>
                                <div className="my-2">
                                    { rating > 0 ?
                                        <>
                                            <strong>Rating: </strong> 
                                            <FontAwesomeIcon className="star-rating" icon={faStar}/> {rating.toFixed(2)}
                                        </>
                                        :
                                        <strong>No Rating </strong> 
                                    }
                                </div>
                                <Row>
                                    <Col className="d-grid">
                                        <AddToCartButton productId={_id}/>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                    </Card.ImgOverlay>
                </div>
            </Card>
        </Col>
    )
}