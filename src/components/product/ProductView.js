import React, { useEffect, useState, useContext } from 'react';
import {Container, Card, Row, Col, Image } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons';

import { UserContext } from '../../UserContext';
import AddToCartButton from '../utils/AddToCartButton';
import { useFetch } from '../utils/FetchHooks';
import BackButton from '../utils/BackButton';
import Error401 from '../errors/Error404';

export default function ProductView()
{
    const {user} = useContext(UserContext);
    const { productId } = useParams();
    const product = useFetch(`${process.env.REACT_APP_API_URL}/products/${productId}`);
    const [rndInt, setRndInt] = useState(0);
    const [name, setName] = useState('');

    useEffect(() => 
    {
        const random = Math.floor(Math.random() * 9) + 1;
        const icedRand = Math.floor(Math.random() * 4) + 1;
        
        if (product !== null) {
            setName(product.name)
        }

        (name.includes('Iced')) ? setRndInt(`iced-${icedRand}`) : setRndInt(random);
        
    }, [rndInt, product, name])

    return(
        (product !== null) ?
        <Container className='mt-5'>
            <Row>
            { product.name === `CastError` ?
                <Error401 />
                :
                <Col>
                    <Row>
                        <Col>
                            <BackButton />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Image fluid src={`/coffee/${rndInt}.png`} />
                        </Col>
                        <Col>
                            <div className="product-view-text p-4 d-flex flex-column justify-content-around">
                                <h1>{name}</h1>
                                <h4>{product.description}</h4>
                                <h3>Ingredients:</h3>
                                <ul>
                                    {product.ingredients.map((item, index) => {
                                        return(
                                            <li key={index}>{item.name}</li>
                                        )
                                    })}
                                </ul>
                                { product.rating > 0 ?
                                        <>
                                            <h2>Rating: 
                                            <FontAwesomeIcon className="star-rating ms-2" icon={faStar}/> {product.rating.toFixed(2)}
                                            </h2>
                                        </>
                                        :
                                        <h3>No Rating Yet</h3>
                                }
                                <div className="d-grid">
                                    <AddToCartButton productId={productId} />
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Col>
            }
            </Row>
        </Container>
        :
        <></>
    ) 
}