import { useContext, useEffect, useState } from 'react';
import {Button, Row, Col, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';

import IngredientsList from '../ingredients/IngredientsList';
import { UserContext } from '../../UserContext';
import { useNavigate } from 'react-router-dom';


export default function UserBrew()
{
    const navigate = useNavigate();
    const {user} = useContext(UserContext);
    const [name, setName] = useState(`${user.firstName} ${user.lastName}'s Brew`),
          [description, setDescription] = useState(''),
          [ingredients, setIngredients] = useState([]),
          [size, setSize] = useState('');

    useEffect(() =>
    {
        fetch(`${process.env.REACT_APP_API_URL}/products/ingredients/available`)
        .then(res => res.json())
        .then(data => setIngredients(data))
        
    }, [ingredients])
   
    function handleAddProduct(e)
    {
        e.preventDefault()

        // get all values of selected ingredients
        const selectedArr = [];
        const selected = document.querySelectorAll('input[type=checkbox]:checked');
        for (var i = 0; i < selected.length; i++) {
            const obj = {name: selected[i].value}
            selectedArr.push(obj)
        }
        console.log(selectedArr)

        if (selectedArr.length > 0)
        {
            fetch(`${process.env.REACT_APP_API_URL}/products`,
            {
                method: 'POST',
                headers: {
                    'Content-type':'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    name: name,
                    description: description,
                    ingredients: selectedArr,
                    size: size
                })
            })
            .then(res => res.json())
            .then(data =>
            {
                console.log(data)
                if (data)
                {
                    Swal.fire({
                        title: `Product created`,
                        icon: 'success',
                        showCloseButton: true
                    })
                    navigate(`/products/${data._id}`)
                    setName('');
                    setDescription('');
                    document.querySelector("#addProductForm");
                }
                else if(data === false)
                {
                    Swal.fire({
                        title: "Duplicate",
                        icon: "warning",
                        text: "Your product name is already registered."
                    })
                }
                else
                {
                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again later."
                    })
                }
            })
        }
        else
        {
            Swal.fire({
                title: "Invalid",
                icon: "warning",
                text: "Product must include ingredients."
            })
        }
    }

    return(
        <Row className="justify-content-md-center">
            <h3 className='text-center'>Brew Your Coffee</h3>
            <Col xs md="8">
                <Form id="addProductForm" onSubmit={(e => handleAddProduct(e))} className="p-4">
                    <Row className='mb-2'>
                        <Col sm className="mb-1">
                            <Form.Control 
                                type="text" 
                                placeholder="Product Name"
                                value={name.toUpperCase()}
                                onChange={e => setName(e.target.value)}
                                required
                            />
                        </Col>
                        <Col sm>
                            <Form.Control 
                                type="text" 
                                placeholder="Description"
                                value={description}
                                onChange={e => setDescription(e.target.value)}
                                required
                            />
                        </Col>
                    </Row>
                    <Row className='mb-2'>
                        <Col md>
                            <IngredientsList items={ingredients} />
                        </Col>
                    </Row>
                    <Row className='mb-2'>
                        <Col md>
                            <Form.Label>Size</Form.Label>
                            <Form.Select size="sm" onChange={e => setSize(e.target.value)}>
                                <option>Sakto</option>
                                <option>Dako</option>
                                <option>Gamay</option>
                            </Form.Select>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="d-grid">
                            <Button className="mr-auto bg-accent border-0 rounded-0" type="submit">Create Coffee Recipe</Button>
                        </Col>
                    </Row>    
                </Form>
            </Col>
        </Row>
    )
}