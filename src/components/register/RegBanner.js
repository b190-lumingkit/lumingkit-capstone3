import { Stack } from 'react-bootstrap';

export default function RegisterBanner()
{
    return(
        <Stack>
            <section className="p-2 text-start">
                <h1>Be One Of Us</h1>
                <h6>After successful registration, you’ll be able to brew your own coffee and share it with fellow kape artisans!</h6>
            </section>
        </Stack>
    )
}