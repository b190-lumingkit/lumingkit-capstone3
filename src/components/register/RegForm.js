import { useState, useEffect, useContext, Fragment } from "react";
import { Navigate, useNavigate, Link, NavLink } from 'react-router-dom';
import { Row, Col, Form, Button, Nav} from 'react-bootstrap';

import jwt_decode from "jwt-decode";

import { UserContext } from '../../UserContext';
import Swal from 'sweetalert2';

export default function RegisterForm()
{
    const {user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState(""),
          [firstName, setFirstName] = useState(""),
          [lastName, setLastName] = useState(""),
          [password1, setPassword1] = useState(""),
          [password2, setPassword2] = useState(""),
          [isInputValid, setIsInputValid] = useState(false);
    const inputs = {
        email: email,
        firstName: firstName,
        lastName: lastName,
        password1: password1
    }
    const navigate = useNavigate()

    useEffect(() =>
    {
        (Object.values(inputs).every(input => input !== "") && password1 === password2) ?
        setIsInputValid(true)
        :
        setIsInputValid(false)
    }, [inputs, password2])

    function registerUser(e)
    {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/users/register`,
        {
            method: 'POST',
            headers: {
                'Content-type':'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password1
            })
        })
        .then(res => res.json())
        .then(data =>
        {
            if (typeof data.duplicateEmail !== 'undefined')
            {
                Swal.fire({
                    title: "Registration failed.",
                    icon: "error",
                    text: "Email already registered."
                })
            }
            else if(typeof data.access !== 'undefined')
            {
                localStorage.setItem('token', data.access)
                const newUser = jwt_decode(data.access);
                setUser(newUser)
                document.querySelector('#registerForm').reset();
                navigate('/');
            }
            else 
            {
                Swal.fire({
                    title: "Something went wrong.",
                    icon: "error",
                    text: "Please try again later."
                })
            }
        })
    }

    return(
        (user.id !== null) ?
        <Navigate to='/' />
        :
        <Fragment>
            <Form id="registerForm" onSubmit={(e => registerUser(e))} className="bg-second p-4">
                <Row className='mb-2 border-0'>
                    <Col>
                        <Form.Control 
                            type="text" 
                            placeholder="First Name"
                            value={firstName}
                            onChange={e => setFirstName(e.target.value)}
                            required
                        />
                    </Col>
                    <Col>
                        <Form.Control 
                            type="text" 
                            placeholder="Last Name"
                            value={lastName}
                            onChange={e => setLastName(e.target.value)}
                            required
                        />
                    </Col>
                </Row>
                <Row className='mb-2'>
                    <Col>
                        <Form.Control 
                            type="email" 
                            placeholder="Email Address"
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                            required
                        />
                    </Col>
                </Row>
                <Row className='mb-2'>
                    <Col sm className="mb-2">
                        <Form.Control 
                            type="password" 
                            placeholder="Password"
                            value={password1}
                            onChange={e => setPassword1(e.target.value)}
                            required
                        />
                    </Col>
                    <Col sm>
                        <Form.Control 
                            type="password" 
                            placeholder="Confirm Password"
                            value={password2}
                            onChange={e => setPassword2(e.target.value)}
                            required
                        />
                    </Col>
                </Row>
                <Row>
                    <Col className="d-grid">
                    <Button className="btn-accent" type="submit">Continue</Button>
                    </Col>
                </Row>    
            </Form>
        </Fragment>        
    )
}