export const cartAPI = `${process.env.REACT_APP_API_URL}/cart`;
export const token = `Bearer ${ localStorage.getItem('token') }`;
export const tokenOnlyHeader = { headers: {Authorization: `Bearer ${localStorage.getItem('token')}`}};