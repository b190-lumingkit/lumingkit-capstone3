import { useNavigate } from 'react-router-dom';
import { Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

export default function BackButton() {

    const navigate = useNavigate();


    return(
        <Row>
            <Col className="p-2">
                <div onClick={() => navigate(-1)} className="back-btn">
                    <FontAwesomeIcon icon={faArrowLeft} />
                    <span className='mx-2'>Return</span>
                </div>
            </Col>
        </Row>
    )
}