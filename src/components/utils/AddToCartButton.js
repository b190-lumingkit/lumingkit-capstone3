// Components
import { useContext } from 'react';
import { Button } from 'react-bootstrap';


import { UserContext } from '../../UserContext';
import { cartAPI } from './FetchUtils';

export default function AddToCartButton(props)
{
    const {user, tempCart, setTempCart} = useContext(UserContext)

    const AddToCart = () =>
    {
        if (user.id !== null)
        {
            fetch(`${cartAPI}/add`,
            {
                method: 'POST',
                headers: { 
                    Authorization: `Bearer ${ localStorage.getItem('token') }`,'Content-Type' : 'application/json'
                },
                body: JSON.stringify({
                    products: [props.productId]
                })
            });
        }
        else
        {
            setTempCart([...tempCart, props.productId]);
        }
    }

    return(
        <Button className="btn-support" onClick={() => AddToCart()}>Add to Cart</Button>
    )
}