// Components
import { Row, Col, Button, Card, Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Error401()
{
    return(
        <Row className="justify-content-sm-center">
            <Col xs md="6">
                <img id="error404svg" src='/404.svg' />
            </Col>
        </Row>
    )
}