import { Navbar, Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCartShopping } from '@fortawesome/free-solid-svg-icons';

export default function AppNavbar()
{
    return(
        <Navbar 
            className="position-sticky top-0 p-3 bg-accent" 
            expand="md"
        >
            <Navbar.Brand>
                <Nav.Link as={ NavLink } to ='/' exact='true' className="text-secondary">Kape ni Angkol</Nav.Link>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="#app-navbar"></Navbar.Toggle>
            <Navbar.Collapse id="#app-navbar" className="justify-content-end">
                <Nav>
                    <Nav.Link 
                        as={ NavLink } 
                        to ='/products' 
                        exact='true'
                        className="text-secondary"
                    > 
                        Products
                    </Nav.Link>
                    <Nav.Link as={ NavLink } to ='/register' exact='true'className="text-secondary">Be One Of Us</Nav.Link>
                    <Nav.Link as={ NavLink } to ='/login' exact='true' className="text-secondary">Login</Nav.Link>
                    <Nav.Link as={ NavLink } to ='/cart' exact='true' className="text-secondary">
                        <FontAwesomeIcon icon={faCartShopping} />
                    </Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}