import { Fragment, useState } from 'react';
import { Form, ListGroup } from 'react-bootstrap';

export default function IngredientsList(props)
{
    const length = props.items.length > 0 ? props.items.length : 9;
    const getFormattedPrice = (price) => ` ${price.toFixed(2)}`;
    const [total, setTotal] = useState(0),
          [checkedState, setCheckedState] = useState(
            new Array(length).fill(false)
          );

    const handleOnChange = (position) => {
        const updatedCheckedState = checkedState.map((item, index) => 
            index === position ? !item: item
        );
        setCheckedState(updatedCheckedState);

        const totalPrice = updatedCheckedState.reduce(
            (sum, currentState, index) => {
                if (currentState === true) {
                    return sum + props.items[index].pricePerServing;
                }
                return sum;
            }, 0);

        setTotal(totalPrice)
    }
   
    return(
        <Fragment>
            <div>
                <Form.Label>Espresso</Form.Label>
                { props.items.map(({name, category}, index) => {
                    if(category === 'coffee') {
                        return(
                            <ListGroup key={index}>
                                <ListGroup.Item className="m-1">
                                    <Form.Check 
                                        type="checkbox"
                                        id={`custom-checkbox-${index}`}
                                        name={name}
                                        value={name}
                                        label={`${name}`}
                                        checked={checkedState[index]}
                                        onChange={() => handleOnChange(index)}
                                    />
                                </ListGroup.Item>
                            </ListGroup>
                        )
                    }
                })}
            </div>
            <div>
                <Form.Label>Milk</Form.Label>
                { props.items.map(({name, pricePerServing, category}, index) => {
                    if(category === 'milk') {
                        return(
                            <ListGroup key={index}>
                                <ListGroup.Item className="m-1">
                                    <Form.Check 
                                        type="checkbox"
                                        id={`custom-checkbox-${index}`}
                                        name={name}
                                        value={name}
                                        label={`${name} P${pricePerServing}`}
                                        checked={checkedState[index]}
                                        onChange={() => handleOnChange(index)}
                                    />
                                </ListGroup.Item>
                            </ListGroup>
                        )
                    }
                })}
            </div>
            <div>
                <Form.Label>Hot/Iced</Form.Label>
                { props.items.map(({name, pricePerServing, category}, index) => {
                    if(category === 'water') {
                        return(
                            <ListGroup key={index}>
                                <ListGroup.Item className="m-1">
                                    <Form.Check 
                                        type="checkbox"
                                        id={`custom-checkbox-${index}`}
                                        name={name}
                                        value={name}
                                        label={`${name} P${pricePerServing}`}
                                        checked={checkedState[index]}
                                        onChange={() => handleOnChange(index)}
                                    />
                                </ListGroup.Item>
                            </ListGroup>
                        )
                    }
                })}
            </div>
            <div>
                <Form.Label>Sweetener</Form.Label>
                { props.items.map(({name, pricePerServing, category}, index) => {
                    if(category === 'sweetener') {
                        return(
                            <ListGroup key={index}>
                                <ListGroup.Item className="m-1">
                                    <Form.Check 
                                        type="checkbox"
                                        id={`custom-checkbox-${index}`}
                                        name={name}
                                        value={name}
                                        label={`${name} P${pricePerServing}`}
                                        checked={checkedState[index]}
                                        onChange={() => handleOnChange(index)}
                                    />
                                </ListGroup.Item>
                            </ListGroup>
                        )
                    }
                })}
            </div>
            <h5 className='py-2'>Total: 
                <span id="totalPrice">{getFormattedPrice(total)}</span>
            </h5>
        </Fragment>
    )
}