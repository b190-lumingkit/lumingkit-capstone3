// Components
import { Row, Col, Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Error401()
{
    return(
        <Col className="pt-5">
            <h1>You are not authorized to access this page.</h1>
        </Col>
    )
}