// Dependencies
import { useContext, Fragment, useEffect, useState } from 'react';
import { UserContext } from '../../UserContext';

// Components
import { Row, Col, ListGroup } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import AddressForm from '../../components/address/AddressForm'

export default function Address()
{
    const { user } = useContext(UserContext)

    return(
        (user.id === null) ?
        <Navigate to='/login' />
        :
        <Row className="formRow align-content-center text-center">
            <h4>Add Address</h4>
            <Col><AddressForm /></Col>
        </Row>
    )
}