import { useContext, useEffect, useState } from 'react';
import { Row, Col, ListGroup, Tabs, Tab, Accordion } from 'react-bootstrap';

import { UserContext } from '../../UserContext';
import AddressModal from '../../components/address/AddressModal';
import OrderLinks from '../../components/orders/OrderLinks';
import UpdateProfileForm from '../../components/user/UpdateProfileFrom';
import UpdateAddressForm from '../../components/address/UpdateAddressForm';

export default function UserProfile()
{
    const {user} = useContext(UserContext);
    const [orders, setOrders] = useState([]);
    const [addressList, setAddressList] = useState([]);

 
    useEffect(() =>
    {
        fetch(`${process.env.REACT_APP_API_URL}/users/orders/my-orders`,
        {
            method: 'GET',
            headers: { Authorization: `Bearer ${localStorage.getItem('token')}`}
        })
        .then(res => res.json())
        .then(data => {
            if (data) {
                let sorted = [...data];
                sorted = [...data].sort((a, b) => new Date(b.purchasedOn) - new Date(a.purchasedOn));

                setOrders(sorted.map(order => {
                    return(
                        <OrderLinks key={order._id} order={order} />
                    )
                }))
            }
        });
    }, [orders])


    function fetchAddress() {
        fetch(`${process.env.REACT_APP_API_URL}/users/addresses`,
        {
            method: 'GET',
            headers: { Authorization: `Bearer ${localStorage.getItem('token')}`}
        })
        .then(res => res.json())
        .then(data => {

            setAddressList(data.map((address, index) =>
            {
                return(
                    <UpdateAddressForm key={index} address={address}/>
                )
            }))
        })
    }

    useEffect(() =>
    {
        fetchAddress()
    }, [addressList]);

    return(
        (user !== null) ?
        <>
        <Row className="justify-content-md-center mt-5">
            <Col xs lg="6">
                <Col className="d-flex justify-content-center">
                    <img id="profilePhoto" src='/cat.jpg' />
                </Col>
            </Col>
        </Row>
        <Row className="justify-content-md-center mt-5">
            <Col xs lg="8">
                <Tabs
                defaultActiveKey="profile"
                id="uncontrolled-tab-example"
                justify
                >
                    <Tab eventKey="profile" title="Profile" className='p-4 bg-white border border-top-0'>
                        <UpdateProfileForm />
                    </Tab>
                    <Tab eventKey="address" title="Address" onClick={() => fetchAddress()} className='p-4 bg-white border border-top-0'>
                        <AddressModal isAdd={true} />
                        { addressList.length > 0 ?
                            <>
                                <h5 className="mt-3 text-center">Your Address</h5>
                                <Accordion>{addressList}</Accordion>
                            </>
                            :
                            <h5 className="mt-5 text-center">You have not added an address.</h5>

                        }
                    </Tab>
                    <Tab eventKey="orders" title="Orders" className='p-4 bg-white border border-top-0'>
                        {   orders.length > 0 ?
                            <ListGroup>{orders}</ListGroup>
                            :
                            <p className="text-center">You have no orders at this time.</p>
                        }
                    </Tab>
                </Tabs>
            </Col>
        </Row>
        </>
        :
        <></>
    ) 
}