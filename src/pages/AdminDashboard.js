import { useEffect, useState } from 'react';
import { Row, Col, ListGroup, Tabs, Tab, Accordion } from 'react-bootstrap';

import AdminAddProduct from '../components/product/AdminAddProduct';
import ProductList from '../components/product/AdminProductList';
import UserList from '../components/user/UserList';
import OrderList from '../components/orders/OrderList';

export default function AdminDashboard()
{
    const [products, setProducts] = useState([]);
    const [users, setUsers] = useState([]);
    const [orders, setOrders] = useState([]);

    const fetchProducts = () =>
    {
        fetch(`${process.env.REACT_APP_API_URL}/products`)
        .then(res => res.json())
        .then(data => setProducts(data))
    }

    const fetchUsers = () =>
    {
        fetch(`${process.env.REACT_APP_API_URL}/admin-panel/all-users`,
        {
            method: 'GET',
            headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
        })
        .then(res => res.json())
        .then(data => setUsers(data))
    }
 
    const fetchOrders = () =>
    {
        fetch(`${process.env.REACT_APP_API_URL}/users/orders/`,
        {
            method: 'GET',
            headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
        })
        .then(res => res.json())
        .then(data => setOrders(data))
    }

    useEffect(() =>
    {
        fetchProducts()
        fetchUsers()
        fetchOrders()
    }, [])

    return(
        <Row className="justify-content-center mt-5">
            <Col xs lg="8">
                <h1 className="text-center">Admin Dashboard</h1>
                <Tabs
                    defaultActiveKey="products"
                    id="uncontrolled-tab-example"
                    justify
                >
                    <Tab eventKey="products" title="Products" className='p-4 bg-white border border-top-0'>
                        <AdminAddProduct />
                        <ProductList products={products} fetchProducts={fetchProducts}/>
                    </Tab>
                    <Tab eventKey="users" title="Users" className='p-4 bg-white border border-top-0'>
                        <UserList users={users} fetchUsers={fetchUsers}/>
                    </Tab>
                    <Tab eventKey="orders" title="Orders" className='p-4 bg-white border border-top-0'>
                        <OrderList orders={orders} fetchOrders={fetchOrders}/>
                    </Tab>
                </Tabs>
            </Col>
        </Row>
    )
}