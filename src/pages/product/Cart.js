// Components
import React, { useContext, useEffect, useState } from 'react';
import { Form, Row, Col, Button, Alert } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import Select from 'react-select';
import Swal from 'sweetalert2';

import { UserContext } from '../../UserContext';
import CartList from '../../components/product/CartList';
import AddressModal from '../../components/address/AddressModal';

export default function Cart()
{
    const navigate = useNavigate();
    const {user, cart, tempCart} = useContext(UserContext);
    const [products, setProducts] = useState([]);
    const [displayProducts, setDisplayProducts] = useState([]);
    const [address, setAddress] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const productCount = {};

    const [paymentMethod, setPaymentMethod] = useState('GCash'),
          [deliveryAddress, setDeliveryAddress] = useState('');
    
    function handleProductCount()
    {
        products.forEach(x => {
            productCount[x] = (productCount[x] || 0) + 1;
        })
    }

    useEffect(() =>
    { 
        (user.id !== null) ? setProducts(cart) : setProducts(tempCart);

        handleProductCount();

        fetch(`${ process.env.REACT_APP_API_URL }/cart/products`, {
            method: "POST",
            headers: {"Content-type": "application/json"},
            body: JSON.stringify({ 
                products: products
            })
        })
        .then(res => res.json())
        .then(data => {
  
            setDisplayProducts(data.map(item => {
                if(productCount.hasOwnProperty(item._id))
                {
                    item.badge = productCount[item._id]
                }
                //console.log(item.badge)
                return item;
            }))
            setIsLoading(false)
        })

    }, [displayProducts])
 
    // print address lines if not empty
    function isLineEmpty(line)
    {
        return line !== '' ? `${line},` : ''; 
    }

    useEffect(() =>
    {
        fetch(`${process.env.REACT_APP_API_URL}/users/addresses`, {
            method: "GET",
            headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
        })
        .then(res => res.json())
        .then(data => {
            if (data.length > 0) {
                setAddress(data.map(item =>
                {
                    return {value: item._id, 
                        label: `${item.name}, ${isLineEmpty(item.line_1)} ${isLineEmpty(item.line_2)} ${isLineEmpty(item.line_3)} ${item.city}, ${item.province}`
                    };
                }));
            }
        })

    }, [address, deliveryAddress])
 
    function handleUpdateCart(newCart)
    {
        fetch(`${ process.env.REACT_APP_API_URL }/cart/update`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-type' : 'application/json'
            },
            body: JSON.stringify({
                products: newCart,
            })
        })
        .then(res => res.json())
        .then(data => console.log(data))
    }

    function handleCheckout()
    {
        //e.preventDefault();

            // get all values of selected ingredients
        const selectedArr = [];
        const selected = document.querySelectorAll('input[type=checkbox]:checked');
        for (var i = 0; i < selected.length; i++) {
            selectedArr.push(selected[i].value)
        }
        
        let productsForCheckout = [];
        let updateCart = [];
        // item.badge = quantity of product/item in user cart
        displayProducts.forEach(item => {
            if (selectedArr.includes(item.name)) {
                // create new Array based on item.badge
                // example: if itemA has item.badge of 2, then result would be [itemA, itemA]
                const itemArr = new Array(item.badge).fill(item.name)
                //merge itemArr with productsForCheckout;
                productsForCheckout = [...productsForCheckout, ...itemArr];
            }
            else {
                updateCart = [...updateCart, item._id];
            }
        });

        //checkout
        if (productsForCheckout.length > 0 && deliveryAddress !== '') {

            fetch(`${ process.env.REACT_APP_API_URL }/users/orders/checkout`, {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                    'Content-type' : 'application/json'
                },
                body: JSON.stringify({
                    products: productsForCheckout,
                    address: deliveryAddress,
                    paymentMethod: paymentMethod
                })
            })
            .then(res => res.json())
            .then(data => {
                if (data._id !== undefined) {
                    Swal.fire({
                        title: "Order is being processed.",
                        icon: "success",
                    })
                    handleUpdateCart(updateCart);
                    navigate(`/orders/${data._id}`);
                }
                else {
                    Swal.fire({
                        title: "Order not successful.",
                        icon: "warning",
                        text: "Something went wrong. Please try again later."
                    })
                }
            });
        } 
        else {
            Swal.fire({
                title: "Checkout invalid.",
                icon: "warning",
                text: "Please check for missing details in your cart."
            })
        }
    }
    
    return(
        (isLoading) ?
        <></>
        :
        (cart.length > 0 || tempCart.length > 0) ?
        <Form id="checkoutForm">
            <h3 className="text-center mt-2">Your Cart</h3>
            {user.id === null ?
                <Row className="justify-content-md-center">
                    <Col sm lg="6" className="p-0">
                        <Alert variant="danger">
                            We currently do not support non-user checkout.
                        </Alert>
                    </Col>
                </Row>
                :
                <></>
            } 
            <CartList items={displayProducts} />
            <Row className="justify-content-md-center">
                <Col xs lg="6" className="mb-2 border p-2">
                    <Form.Label>Deliver to:</Form.Label>
                        <Select 
                            className="mb-2"
                            placeholder="Select Address..."
                            options={address} 
                            onChange={(e) => setDeliveryAddress(e.value)}
                        />
                        <AddressModal />
                </Col>
            </Row>
            <Row className="justify-content-md-center">
                <Col xs lg="6" className="mb-2 border p-2">
                    <Form.Label>Payment Method:</Form.Label>
                    <Form.Select value={paymentMethod} onChange={(e) => setPaymentMethod(e.target.value)}>
                        <option>GCash</option>
                        <option>COD</option>
                    </Form.Select>
                </Col>
            </Row>
            <Row className="justify-content-md-center">
                <Col xs lg="6" className="d-flex justify-content-end">
                    <Button 
                        variant="primary" 
                        type="button"
                        onClick={() => handleCheckout()}
                    >
                    Checkout
                    </Button>
                </Col>
            </Row>
        </Form>
        :
        <>
            <Row className="justify-content-sm-center">
                <Col xs lg="6" className="d-flex justify-content-center">
                    <img src='/emptycart.svg' fluid />
                </Col>
            </Row>
            <Row className="justify-content-sm-center">
                <Col>
                    <Link to='/products' className='text-decoration-none'>
                        <h1 className="text-accent text-center">Get some coffee <FontAwesomeIcon icon={faCoffee} /></h1>
                    </Link>
                </Col>
            </Row>
        </>
    )
}