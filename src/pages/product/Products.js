// Dependencies
import { useState, useEffect} from 'react';

// Components
import { Row, Col } from 'react-bootstrap';
import ProductCard from '../../components/product/ProductCard';
import BackButton from '../../components/utils/BackButton';

export default function Register()
{
    const [products, setProducts] = useState([]);

    useEffect(() =>
    {
        fetch(`${process.env.REACT_APP_API_URL}/products/available`)
        .then(res => res.json())
        .then(data => {
            setProducts(data.map(product => {
                const random = Math.floor(Math.random() * 4) + 1;
                let imgSource = '';
                if (product.name.includes('Iced')) {
                    imgSource = `/coffee/iced-coffee${random}.jpg`;
                }
                else {
                    imgSource = `/coffee/coffee${random}.jpg`;
                }
                return(
                    <ProductCard key={product._id} productProp={product}></ProductCard>
                )
            }))
        })
    }, [products])

    return(
        <Row className="justify-content-md-center">
            <Row>
                <Col className="sticky-top">
                    <BackButton />
                </Col>
            </Row>
            {products}
        </Row>
    )
}