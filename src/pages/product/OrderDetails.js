import { useEffect, useState } from "react";
import { useParams } from 'react-router-dom';
import {Button, Row, Col} from 'react-bootstrap';
import AddProductReview from '../../components/orders/AddProductReview';
import BackButton from '../../components/utils/BackButton';

import { useFetch } from '../../components/utils/FetchHooks';

export default function OrderDetails()
{
    const [productNames, setProductNames] = useState('');
    const { orderId } = useParams();
    const order = useFetch(`${process.env.REACT_APP_API_URL}/users/orders/${orderId}`);

    useEffect(() => {
        
        if (order !== null) {
            setProductNames(order.products.map(product => product.name))
        }

    }, [order])

    function isLineEmpty(line)
    {
        return line !== '' ? `${line},` : ''; 
    }

    return(
        (order !== null) ?
        <>
        <Row id="orderDetails" className="justify-content-md-center mt-5">
                <Row className="justify-content-md-center">
                    <Col xs md="6" lg="4">
                        <BackButton />
                    </Col>
                </Row>
                <Col xs md="6" lg="4" className="mb-2 border p-5 border-dark bg-white">
                    <h3 className='text-center'>Order Details</h3>
                    <p>Order ID: {order._id}</p>
                    <ul>
                        {order.products.map((product, index) => {
                            return(
                                <li key={index} className="d-flex justify-content-between">
                                    <span>{product.name}</span>
                                    <span>{product.price.toFixed(2)}</span>
                                </li>
                            )
                        })}
                    </ul>
                    <p>Total: {order.totalPrice.toFixed(2)}</p>
                    <p>Address:
                        {` ${order.address.name}, 
                        ${isLineEmpty(order.address.line_1)}
                        ${isLineEmpty(order.address.line_2)}
                        ${isLineEmpty(order.address.line_3)}
                        ${order.address.city},
                        ${order.address.province}
                        `}
                    </p>
                    <p>Status: {order.status[0].toUpperCase() + order.status.slice(1)}</p>
                    <p>Payment Method: {order.paymentMethod}</p>
                </Col>
            <Row className="justify-content-md-center mt-5">
                <Col xs md="6" lg="4" className='d-grid p-0'>
                    <AddProductReview orderId={orderId} productNames={productNames}/>
                </Col>
            </Row>
            
        </Row>
        
        </>
        :
        <></>
    ) 
}