// Dependencies
import { useState, useEffect} from 'react';

// Components
import { Row, Col } from 'react-bootstrap';
import FeaturedCard from '../../components/product/FeaturedCard';
import BackButton from '../../components/utils/BackButton';


export default function Featured()
{
    const [products, setProducts] = useState([]);

    useEffect(() =>
    {
        fetch(`${process.env.REACT_APP_API_URL}/products/available`)
        .then(res => res.json())
        .then(data => {
            setProducts(data.map((product, index) => {
                if (product.rating > 0) {
                    const random = Math.floor(Math.random() * 4) + 1;
                    let imgSource = '';
                    if (product.name.includes('Iced')) {
                        imgSource = `/coffee/iced-coffee${random}.jpg`;
                    }
                    else {
                        imgSource = `/coffee/coffee${random}.jpg`;
                    }
                    return(
                        <FeaturedCard key={product._id} productProp={product}></FeaturedCard>
                    )
                }
            }))
        })
    }, [products])

    return(
        <>
        <Row className="justify-content-md-center">
            <Row>
                <Col>
                    <BackButton />
                </Col>
            </Row>
            <Row className='d-flex align-items-center'>
                <h1 className="text-center">Featured This Week</h1>
                {products}
            </Row>
                
        </Row>
        </>
    )
}