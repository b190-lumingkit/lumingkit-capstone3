// Components
import { Row, Col} from 'react-bootstrap';
import RegisterForm from '../components/register/RegForm';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHandshake } from '@fortawesome/free-solid-svg-icons';

export default function Register()
{
    return(
        <Row className="formRow align-content-center justify-content-center">
            <Col lg="6" xl="4">

                <h1 className="text-accent text-center">Be One Of Us! <FontAwesomeIcon icon={faHandshake} /></h1>
                <h5 className='text-center'>After successful registration, you can brew your own coffee and share it with fellow kape artisans!</h5>
                <RegisterForm />
            </Col>
        </Row>
    )
}