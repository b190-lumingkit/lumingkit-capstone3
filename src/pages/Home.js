// Dependencies
import { useContext, Fragment} from 'react';
import { useNavigate } from 'react-router-dom';

// Components
import { Row, Col, Image, Container } from 'react-bootstrap';

export default function Register()
{
    const navigate = useNavigate();

    function onClick(route) {
        navigate(route)
    }

    return(
        <Container className="home-container">
            <Row>
                <Col className='p-0' onClick={() => onClick('/products/popular')}>
                    <Image src="/coffee-popular.gif" fluid/>
                </Col>
                <Col className='p-0' onClick={() => onClick('/products/featured')}>
                    <Image src="/coffee-featured.gif" fluid/>
                </Col>
            </Row>
            <Row>
                <Col className='p-0 brew' onClick={() => onClick('/brew-your-coffee')}>
                    <Image src="/coffee-brew.gif" fluid/>
                </Col>
            </Row>
        </Container>
    )
}