// Components
import { Row, Col } from 'react-bootstrap';
import LoginForm from '../components/login/LoginForm';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMugHot } from '@fortawesome/free-solid-svg-icons';

export default function Login()
{
    return(
        <Row className="formRow align-content-center justify-content-center">
            <Col lg="6" xl="4">
                <h1 className="text-accent text-center">Start brewing... <FontAwesomeIcon icon={faMugHot} /> </h1>
                <LoginForm />
            </Col>
        </Row>
    )
}