import { Navigate} from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
import { UserContext } from '../UserContext';

export default function Logout()
{
    const { user, unsetUser } = useContext(UserContext);
    
    useEffect(() => {
        unsetUser();
    }, [])

    return (    
        <Navigate to='/login' />
    )
}